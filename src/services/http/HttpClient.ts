import axios, { AxiosResponse } from 'axios';


class HttpClient {

    public OnAuthFault!: Function;

    public OnJwtRefreshed: Function[];

    public baseUrl:string=process.env.NODE_ENV === 'development' ? 'https://localhost:5001/' : 'https://www.api.inclus-dev.ru/';

    public  Url: string = this.baseUrl +'api/';

    private IsRetried: boolean;

    private token: string|undefined;

    constructor() {
        this.IsRetried = false;
        this.OnJwtRefreshed = new Array<Function>();
    }

    public async GetDictionary<T>(fileName:string):Promise<T>
    {
        let response = await axios.get(this.baseUrl+'static/dictionaries/'+fileName)
        return Promise.resolve(response.data as T);
    }

    public async Post(uri: string, data?: object ): Promise<AxiosResponse> {

        return await this.SendRequest('post', uri, data);

    }

    public async Delete(uri: string, query?: string): Promise<AxiosResponse> {
       return await this.SendRequest('delete', uri, undefined, query);
    }


    public async Get(uri: string, query?: string): Promise<AxiosResponse> {

        return await this.SendRequest('get', uri, undefined, query);
    }

    public async Put(uri: string, data?: object): Promise<AxiosResponse> {
        return await this.SendRequest('put', uri, data);
    }

    public SetToken(token: string) {
        this.token = token;
    }

    private async RefreshToken() {
        const response = await this.Put('access');
        if (response.status === 200) {
            this.OnJwtRefreshed.forEach((x:Function) => {
                x(response.data.token);
            });
        }
    }

    private async SendRequest(type: string, uri: string, data?: object, query?: string): Promise<AxiosResponse> {

        let completeUri = uri;
        let completeData = null;
        if (type === 'get' || type == 'delete') {
            completeUri += (query !== undefined ? ('?' + query) : '');
        } else {
            completeData = data;
        }
        try {
        const axiosResponse: AxiosResponse =
        await axios.request<AxiosResponse>({url: this.Url + completeUri, method: type, data: completeData
            , headers: {Authorization: 'Bearer ' + this.token}});
        return Promise.resolve(axiosResponse);
        } catch (ex) {
                if (ex.response == undefined) {
                throw ex;
            }
            let status:number=ex.response.status;
                if (status == 401 || status==403) {
                if (!this.token) {
                    this.OnAuthFault();
                } else {
                if (!this.IsRetried) {
                this.RefreshToken();
                return await this.SendRequest(type, uri, data, query);
                } else {
                    this.OnAuthFault();
                    throw ex;
                }
            }

            }else{
                throw ex;
            }
        }
    }
}
const Http = new HttpClient();
export default Http;
