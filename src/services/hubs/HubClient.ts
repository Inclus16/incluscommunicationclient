import {HubConnection,HubConnectionBuilder, HttpTransportType} from "@microsoft/signalr";
import HttpClient from '@/services/http/HttpClient';
import Message from '@/models/hubs/Message';

class HubConnectionsClient
{
    private messages!:HubConnection;

    private jwt:string='';

    public onNewMessage:CallableFunction[]= new Array<CallableFunction>();

    public onError:CallableFunction[]=new Array<CallableFunction>();

    public initConnections():void
    {
        this.messages = new HubConnectionBuilder()
        .withUrl(HttpClient.baseUrl+"hubs/messages",{ accessTokenFactory: () => this.jwt, skipNegotiation: true, transport: HttpTransportType.WebSockets})
        .withAutomaticReconnect()
        .build()
    }

    public configureConnections():void
    {
	
        this.messages.on('Receive',(message:Message)=>{
            this.onNewMessage.forEach((callable:CallableFunction)=>{
                callable(message);
            });
        });

        this.messages.on('Error',(error:string)=>{
            this.onError.forEach((callable:CallableFunction)=>{
                callable(error);
            })
        })
    }

    public async send(message:Message)
    {
       await this.messages.send('Send',{receiverId:message.receiverId,body:message.body,createdAt:message.createdAtString});
    }

    public setJwt(jwt:string)
    {
        this.jwt=jwt;
        if(this.messages!=undefined){
            this.messages.stop();
        }
        this.initConnections();
        this.configureConnections();
        this.messages.start().catch(err => console.log(err));
    }
}

const HubClient = new HubConnectionsClient();
export default HubClient;
