export default class DateTimeHelper
{
    public static GetValidServerDateTimeFromDate(date:Date):string
    {
        return date.toLocaleString('ru-RU').replace(/\./g,'-').replace(',','')
    }

    public static GetValidServerDateTimeFromString(date:string):string
    {
        return (new Date(date)).toLocaleString('ru-RU').replace(/\./g,'-').replace(',','')
    }
}