import Vue from 'vue';
import { Dictionary } from 'vue-router/types/router';
import HubClient from '../hubs/HubClient';

export default class NotificationHelper
{

    public static subscribeNewMessages(func:Function)
    {
        HubClient.onNewMessage.push(func);
    }

    public static subscribeError(func:Function)
    {
        HubClient.onError.push(func);
    Vue.config.errorHandler = function(err: Error, vm: Vue, info: string) {
      switch (err.message) {
        case 'Network Error':
          func('Ошибка соединения с сервером');
          break;
         case 'Request failed with status code 400':
           const errors: Dictionary<string> = err.response.data.errors;
           for (const key of Object.keys(errors)) {
                func(errors[key][0]);
              }
           break;
         case 'Request failed with status code 403':
         case 'Request failed with status code 401':
           func('Ошибка авторизации');
           break;
         default:
           if(process.env.NODE_ENV === 'development'){
              console.log(err);
           }
           func('Неизвестная ошибка');
           break;
      }
    }.bind(this);
    }
}