import HttpClient from '../http/HttpClient';
import * as Cookie from 'es-cookie';
import AuthorizedUser from '@/models/security/AuthorizedUser';
import HubClient from '../hubs/HubClient';
class SecurityProvider {

    public OnAuthFault: Function[] = new Array<Function>();
    private jwt: string|undefined;

    private currentUser: AuthorizedUser = new AuthorizedUser('', '', 0);

    public constructor() {
        this.init();
        HttpClient.OnJwtRefreshed.push(this.setJwt.bind(this));
        HttpClient.OnAuthFault = () => {
            this.OnAuthFault.forEach((x) => x());
        };
    }

    public isJwtSet(): boolean {
        return this.jwt !== undefined && this.jwt.trim().length > 0;
    }

    public setJwt(jwt: string): void {
        this.jwt=jwt;
        HttpClient.SetToken(jwt);
        HubClient.setJwt(jwt);
        Cookie.set('jwt', jwt);
        this.paseJwt(jwt);
    }

    public forgetJwt() {
        this.jwt = undefined;
        Cookie.remove('jwt');
    }

    public getCurrentUser(): AuthorizedUser {
        return this.currentUser;
    }

    private init(): void {
        this.jwt = Cookie.get('jwt');
        if (this.isJwtSet()) {
            this.setJwt(this.jwt);
        }
    }

    private paseJwt(jwt: string): void {
         const base64Url = jwt.split('.')[1];
         const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
         const jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
                           return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                         }).join(''));

         const payload = JSON.parse(jsonPayload);
         this.currentUser = new AuthorizedUser(
            payload[AuthorizedUser.EMAIL_SCHEMA].toString(),
            payload[AuthorizedUser.NAME_SCHEMA].toString(),
            parseInt(payload[AuthorizedUser.ID_SCHEMA]));
    }
}
const sp = new SecurityProvider();
export default sp;
