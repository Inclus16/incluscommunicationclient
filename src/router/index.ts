import Vue from 'vue';
import VueRouter, { Route } from 'vue-router';
import Home from '../views/Home.vue';
import SecurityProvider from '@/services/security/SecurityProvider';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/access',
    name: 'access',
    component: () => import(/* webpackChunkName: "access" */ '../views/Access.vue'),
  },
  {
    path: '/users',
    name: 'users',
    component: () => import(/* webpackChunkName: "users" */ '../views/Users.vue'),
  },
  {
    path: '/chats',
    name: 'chats',
    component: () => import(/* webpackChunkName: "chats" */ '../views/Chats.vue'),
  },
  {
    path: '/chat/:id',
    name: 'chat',
    component: () => import(/* webpackChunkName: "chat" */ '../views/Chat.vue'),
  },
];

const router = new VueRouter({
  routes,
});
router.beforeEach((to: Route, from: Route, next: Function) => {
 if (to.name !== 'access') {
   if (SecurityProvider.isJwtSet()) {
     next();
   } else {
     next('/access');
   }
 } else {
   next();
 }
});

SecurityProvider.OnAuthFault.push(() => router.push('access'));

export default router;
