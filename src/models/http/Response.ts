export default class Response<T> {
    public status!: number;

    public data!: T;
}
