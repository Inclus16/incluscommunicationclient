import User from '../User';

export default class Message{

    public id!:number;

    public body!:string;

    public createdAt!:Date;


    public createdAtString!:string;


    public senderId!:number;

    public receiverId!:number;

    public receiver!:User;
    
    public sender!:User;
}