export default class BasicNotification
{
    public title:string|null=null;

    public text:string|null=null;

    public imagePath:string|null=null;

    public onClick:Function|null=null;

}
