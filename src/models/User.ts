export default class User {
    public id!: number;

    public email!: string;

    public name!: string;

    public createdAt!: Date;

    public createdAtString!: string;
}
