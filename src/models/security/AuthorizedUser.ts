export default class AuthorizedUser {

    public static readonly EMAIL_SCHEMA: string = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress';

    public static readonly NAME_SCHEMA: string = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name';

    public static readonly ID_SCHEMA: string = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier';

    public email: string;

    public id: number;

    public name: string;

    public constructor(email: string, name: string, id: number) {
        this.email = email;
        this.name = name;
        this.id = id;
    }
}
