export default class Emoji
{
    public Codes!:string;

    public Char!:string;

    public Name!:string;

    public Category!:string;
}